# Software I use   
    
In general I want to use my software rather then being used by the software. It must behave the way I configured it and do no more or less then it's supposed to do.    
    
## Root   
   
### Operating System/Distribution   
   
* Manjaro Linux - An Arch based Linux distribution with a rolling release and quick installation. Half an hour after deleting an old OS I've installed and setup a fully new installation and my system ist as it was before.   
   
### Terminal emulator   
   
* Alacritty - I like URXVT as well but it lacks of nativ scaling.   
   
### Shell   
   
* Fish - I know it's not POSIX compliant but I just love its features out-of-the-box.   
   
### Window Manager   
   
* i3wm - along with i3bar and bumblebee-status. Easy to setup and configure, no need of re-compiling. Inplace reload config and restart wm.   
* spectrwm - another window manager I use ocationally. Inplace reload config and restart wm.   
   
### Hotkey daemon   
   
* sxhkd - along with the window manager to keep things modular. Here I keep application and navigation bindings.   
   
### Editor   
   
* Vim - A powerful editor which I use for scripting, notes, configs, .. basically for everything.   
   
### Web browser   
   
* Vivaldi - I'm not a hardcore OSS envangelist, for me those 5 percent closed source -  for the UI - is aceptable. I can configure this browser exactly the way I want it.   
* Torbrowser - ocationally.   
   
### Backup
   
* Borg - for home   
* Timeshift - for system   
      
## Utilities   
   
### File manager   
   
* Ranger - My Terminal FM with vim-keys   
* PcmanFM - My GUI FM   
   
### Mail client   
   
* Vivaldi - Not really UNIX philosophy but I just like to have Mail, RSS, Calendar, Contacts in one place.   
   
### RSS   
   
* Vivaldi - Not really UNIX philosophy but I just like to have Mail, RSS, Calendar, Contacts in one place.   
* Newsboat - Ocationally for terminal   
   
### Calendar   
   
* Vivaldi - Not really UNIX philosophy but I just like to have Mail, RSS, Calendar, Contacts in one place.   
   
### Contacts   
   
* Vivaldi - Not really UNIX philosophy but I just like to have Mail, RSS, Calendar, Contacts in one place.   
   
### Radio player   
   
* Curseradio - Terminal based Webradio   
   
### Video player   
   
* mpv - Video player with no UI   
   
### Office   
   
* I don't have a office suit installed as I write mostly in **markdown**. If I do need it I open Onlyoffice from my selfhosted Nextcloud instance.   
   
### Image editing   
   
* Gimp - Easy, powerful   
   
## Server   
   
### 1   
   
* Debian - Dedicated purely to host my Nextcloud instance; syncing Instant messaging and Video calls, Contacts, Calendar, Notes, Tasks, Polls, Kanban project management.   
   
### 2   
   
* Dietpi - Raspberry to serve VPN, DNS + Ad-Blocker for all devices in my network, ddns-updates.   
   
## Mobil   
   
* LineageOS - I refused a "smartphon" till 2019, with this OS I fond somewhat a compromise.   
* Pinephone - to play around a bit. Someday might be my daily driver but I don't like the form factor as I prefer smaller devices which don't fill my whole pocket.   
    
rauldux, 2021    
