# ADB Backup

## BACKUP:

* BACKUPDIR="~/"
* adb root
* adb pull -a data/user/0 ${BACKUPDIR}
* adb pull -a data/app ${BACKUPDIR}
* adb pull -a mnt/sdcard ${BACKUPDIR}

## RESTORE:

* adb kill-server
* adb root
* adb push ${BACKUPDIR}/sdcard /mnt/
* time find ${BACKUPDIR}/app/ -type f -name base.apk -print0 -exec adb install {} ;
* adb push ${BACKUPDIR}/0 /data/user/
* adb shell

## Generate a permissions fixer from /data/system/packages.list:

* cat /data/system/packages.list | awk '{print "chown u0_a" $2-10000 ":u0_a" $2-10000 " /data/data/"$1" -R"}' > /data/media/fix_perms.sh
* sh /data/media/fix_perms.sh
* rm /data/media/fix_perms.sh
* exit
* adb reboot

rauldux, 2021
