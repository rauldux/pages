# rcm — dotfile management
## SYNOPSIS
lsrc	

mkrc	

rcdn	

rcup	
## DESCRIPTION
The rcm suite of tools is for managing dotfiles directories. This is a directory containing all the .*rc files in your home directory (.zshrc, .vimrc, and so on). These files have gone by many names in history, such as “rc files” because they typically end in rc or “dotfiles” because they begin with a period.
This suite is useful for committing your rc files to a central repository to share, but it also scales to a more complex situation such as multiple source directories shared between computers with some host-specific or task-specific files.
This guide serves as a tutorial motivating the suite. For a list of quick reference documentation see the SEE ALSO section below.
## QUICK START FOR EXISTING DOTFILES DIRECTORIES
This section is for those who already have an existing dotfiles directory; this directory is ~/.dotfiles; the directory only contains rc files; and these rc filenames do not begin with a period. See the caveats below if this is not you.
Dry run with lsrc(1). Look for anything unexpected in here, such as ~/.install or ~/.Makefile, or an empty list of dotfiles.
lsrc
Update any symlinks with rcup(1). This is likely to do nothing, since your dotfiles already exist.
rcup -v
When necessary, add new rc files to the dotfiles directory with mkrc(1).
mkrc ~/.tigrc
In the other direction, you can use rcup(1) to create the symlinks from ~/.dotfiles to your home directory.
rcup tigrc
### COMMON PROBLEM: EXISTING INSTALL SCRIPTS
Many existing dotfile directories have scripts named install or Makefile in the directory. This will cause a ~/.install or ~/.Makefile symlink to be created in your home directory. Use an exclusion pattern to ignore these.
rcup -x install -x Rakefile -x Makefile -x install.sh
### COMMON PROBLEM: DOTTED FILENAMES IN DOTFILES DIRECTORY
A less common situation is for all the filenames in your dotfiles directory to be prefixed with a period. These files are skipped by the rcm suite, and thus would result in nothing happening. The only option in this case is to rename all the files, for example by using a shell command like the following.
find ~/.dotfiles -name '.*' -exec echo mv {} `echo {} | sed 's/.//'` ;
Note that this will break any existing symlinks. Those can be safely removed using the rcdn(1) command.
rcdn -v
### COMMON PROBLEM: DOTFILES DIRECTORY NOT IN ~/.dotfiles
This all assumes that your dotfiles directory is ~/.dotfiles. If it is elsewhere and you do not want to move it you can use the -d DIR option to rcup(1) or modify DOTFILES_DIRS in rcrc(5).
rcup -d configs -v
### COMMON PROBLEM: CONFIGURATION FILES/DIRECTORIES WITHOUT DOTS
By default, the rcm suite will prefix every file and directory it manages with a dot. If that is not desired, for example in the case of ~/bin or ~/texmf, you can add that file or directory to UNDOTTED in rcrc(5) or use the -U option. For example:
mkrc -U bin
## QUICK START FOR EMPTY DOTFILES DIRECTORIES
This section is for those who do not have an existing dotfiles directory and whose dotfiles are standard.
Add your rc files to a dotfiles directory with mkrc(1).
mkrc .zshrc .gitconfig .tigrc
Synchronize your home directory with rcup(1)
rcup -v
This will give you a directory named ~/.dotfiles with your dotfiles in it. Your original dotfiles will be symlinks into this directory. For example, ~/.zshrc will be a symlink to ~/.dotfiles/zshrc.
## TAGGED DOTFILES
This suite becomes more powerful if you share your dotfiles directory between computers, either because multiple people share the same directory or because you have multiple computers.
If you share the dotfiles directory between people, you may end up with some irrelevant or even incorrect rc files. For example, you may have a .zshrc while your other contributor has a .bashrc. This situation can be handled with tags.
A tag is a directory under the dotfiles directory that starts with the letters tag-. We can handle the competing shell example by making a tag-zsh directory and moving the .zshrc file into it using mkrc(1) and passing the -t option.
mkrc -t zsh .zshrc
When updating with rcup(1) you can pass the -t option to include the tags you want. This can also be set in the rcrc(5) configuration file with the TAGS variable.
rcup -t zsh
## MULTIPLE DOTFILE DIRECTORIES
Another common situation is combining multiple dotfiles directories that people have shared with you. For this we have the -d flag or the DOTFILES_DIRS option in .rcrc.
The following rcup invocation will go in sequence through the three dotfiles directories, updating any symlinks as needed. Any overlapping rc files will use the first result, not the last; that is, .dotfiles/vimrc will take precedence over marriage-dotfiles/vimrc.
rcup -d .dotfiles -d marriage-dotfiles -d thoughtbot-dotfiles
An exclusion pattern can be tied to a specific dotfiles directory.
rcup -d .dotfiles -d work-dotfiles -x 'work-dotfiles:powrc'
## HOST-SPECIFIC DOTFILES
You can also mark host-specific files. This will go by the hostname. The rcrc(5) configuration file is a popular candidate for a host-specific file, since the tags and dotfile directories listed in there are often specific to a single machine.
mkrc -o .rcrc
If your hostname is difficult to compute, or you otherwise want to use a different hostname, you can use the -B flag.
mkrc -B eggplant .rcrc
## AUTHORS
rcm is maintained by Mike Burns <mburns@thoughtbot.com> and thoughtbot
